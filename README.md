Ansible_gpu_exporter
=========

Exporter for GPU metrics

Requirements
------------

- NVIDIA Data Center GPU Manager (DCGM)， https://developer.nvidia.com/dcgm
- Grafana Dashboard : https://grafana.com/grafana/dashboards/11752

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

ansible_node_exporter role

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      tasks:
          - include_role:
                name: ansible_node_exporter


